/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: facebookSourceController
 * @description
 * #facebookSourceController
 * It is a Facebook source controller.
 * This controller is responsible for showing the details of the page.
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 * @requires $state
 * @requires Flash
 * @requires formatdateService
 * @requires $filter
 * @requires $stateParams
 * @requires exportToExcelService
 * 
 * @property {String} current : String, This holds current page name.
 * @property {array} rowLimitOptionArray : array This holds entries in dropdown options.
 * @property {String} selected : number, default set from rowLimitOptionArray[1].
 * @property {array} facebookSourceList: array this holds all details displayed in Facebook source page.
 */

app.controller("facebookSourceController", function ($rootScope, $scope, $http, $state, Flash, $stateParams, $filter, exportToExcelService, formatdateService) {
	var facebookSourceList;
	$scope.menu.current = "facebookSource";

	//OptionValues	
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	};

	/**
	 * Get all facebook source list.
	 * @description - Getting facebook source list.		
	 * @callback requestCallback
	 * @param {requestCallback} response - The callback that handles the response.
	 * @param {list} response.data - List of facebook source.
	 */
	var successCallBack = function (response) {
		$scope.facebookSourceList = response.data;
		$scope.facebookSourceList = formatdateService.formatdate(response.data);
		$scope.exportInfo = angular.copy(response.data);
	}

	/**
	 * Get Error information.	
	 * @description - Gives error information.	
	 * @callback requestCallback
	 * @param {requestCallback} reason - The callback that handles the response.
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	}

	/**
	 * Asynchronous rest call
	 * @description - http GET request to get all facebook source. 
	 * @param {string} status - status of source (active,inactive).
	 */
	getSourceList = function (status) {
		var _url;
		if (status == 'active')
			_url = 'source/facebook/active';
		else if (status == 'inactive')
			_url = 'source/facebook/inactive';
		else
			_url = 'source/facebook';
		$http({
				method: "GET",
				url: _url
			})
			.then(successCallBack, errorCallBack);
	}
	getSourceList($stateParams.status); //on load
	$scope.orderStatus = $stateParams.status;

	/**
	 * Ordering source by status
	 * @description - Getting source list according to keyword status as active, inactive, both.
	 * @param {string} status - status of source(active,inactive,all)
	 */
	$scope.orderByStatus = function (status) {
		$state.go('homeParent.facebookSource', {
			status: $scope.orderStatus
		});
	}

	/** 
	 * Reseting pop up values
	 * @description - Reset source details in pop-up window after adding source or exiting pop-up window.
	 */
	$scope.reset = function () {
		$scope.facebookSourceType = null;
		$scope.facebookSourceName = null;
		$scope.confirmSubmit = null;
	}

	/**
	 * Addition of source
	 * @description - Adding source name and type.
	 * @param {string} _Name - Source name. 
	 * @param {string} _Type - Source type. 
	 * @param {string} orderStatus - Status of keyword.
	 */
	$scope.addingSource = function (_Name, _Type, orderStatus) {
		var facebookSource = {
			"sourceName": _Name,
			"sourceTypeName": _Type
		};
		$http({
				method: "POST",
				url: 'source/facebook',
				data: facebookSource
			})
			.then(function (response) {
				getSourceList($stateParams.status);
				// user role implementation messages
				if ($rootScope.userRole == 'super_user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> is successfully added';
					var id = Flash.create('success', message);
				} else if ($rootScope.userRole == 'user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> addition is pending for approval';
					var id = Flash.create('success', message);
				}

			}, function (reason) {
				var message = '<strong>' + _Name.toUpperCase() + '</strong> is unable to add , might be due to duplicates.';
				var id = Flash.create('danger', message);
				$scope.error = reason.data;
			}); //add
		$('#facebookSourceModal').modal('hide');
	}

	/**
	 * Changing status of source to Inactive.
	 * @description - Inactivating current source. 
	 * @param {object} _source - Contains single source details. 
	 */
	$scope.inactiveStatus = function (_source) {
		_source.disabled = true
		var facebookSource = {
			"sourceName": _source.sourceName,
			"sourceTypeName": _source.sourceTypes.sourceType
		};
		$http({
			method: "PUT",
			url: 'source/facebook/inactive/' + _source.id,
			data: facebookSource
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is succesfully deactivated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> deactivation is pending for approval ';
				var id = Flash.create('success', message);
			}

		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is unable to deactivate';
			var id = Flash.create('danger', message);
		});
	};

	/**
	 * Changing status of source to Active.
	 * @description - Activating current source. 
	 * @param {object} _source - Contains single source details. 
	 */
	$scope.activateStatus = function (_source) {
		_source.disabled = true
		var facebookSource = {
			"sourceName": _source.sourceName,
			"sourceTypeName": _source.sourceTypes.sourceType
		};
		$http({
			method: "PUT",
			url: 'source/facebook/active/' + _source.id,
			data: facebookSource
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is succesfully activated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> activation is pending for approval ';
				var id = Flash.create('success', message);
			}

		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _source.sourceName.toUpperCase() + '</strong> is unable to activate';
			var id = Flash.create('danger', message);
		});
	};

	/** 
	 * Export to Excel
	 * @description - Export to Excel all facebook source.
	 */
	$scope.export = function () {
		var table = $scope.exportInfo;
		table = $filter('orderBy')(table, 'sourceName');
		var csvString = '<table><tr><td>Source</td><td>Type</td><td>Created By</td><td>Last Modified At</td><td>Status</td></tr>';
		var dateInfo, statusInfo;
		for (var i = 0; i < table.length; i++) {
			var rowData = table[i];
			var d = new Date();
			d.setTime(rowData.startDate);
			dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
			if (rowData.status == true)
				statusInfo = 'Active';
			else
				statusInfo = 'Inactive';
			csvString = csvString + "<tr><td>" + rowData.sourceName + "</td><td>" + rowData.sourceTypes.sourceType + "</td><td>" + rowData.requestedBy + "</td><td>" + dateInfo + "</td><td>" + statusInfo + "</td>";
			csvString = csvString + "</tr>";
		}
		csvString += "</table>";
		csvString = csvString.substring(0, csvString.length);
		exportToExcelService.converttoExcel(csvString, 'Facebook_Source');
	}
	/** 
	 * Export to pdf
	 * @description - Export to pdf all facebook source.
	 */
	$scope.exportpdf = function () {
		{
			var item = $scope.exportInfo;
			item = $filter('orderBy')(item, 'sourceName');
			var doc = new jsPDF();
			var col = ["Source", "Type", "Created By", "Last Modified At", "Status"];
			var rows = [];
			var dateInfo, statusInfo;
			for (var i = 0; i < item.length; i++) {
				var rowData = item[i];
				var d = new Date();
				d.setTime(rowData.startDate);
				dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
				if (rowData.status == true)
					statusInfo = 'Active';
				else
					statusInfo = 'Inactive';
				var temp = [rowData.sourceName, rowData.sourceTypes.sourceType, rowData.requestedBy, dateInfo, statusInfo];
				rows.push(temp);
			}
			doc.autoTable(col, rows);
			doc.save('Facebook_Source' + new Date().toDateString() + '.pdf');
		}
	};
});