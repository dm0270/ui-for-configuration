/**
 * @author Pushpa Gudmalwar
 * @name webApp.controller: facebookKeywordController
 * @description
 * #facebookKeywordController
 * It is a Facebook keyword controller.
 * This controller is responsible for showing the details of the page.
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 * @requires $state
 * @requires Flash
 * @requires formatdateService
 * @requires $filter
 * @requires $stateParams
 * @requires exportToExcelService
 * 
 * @property {String} current : String, This holds current page name.
 * @property {array} rowLimitOptionArray : array This holds entries in dropdown options.
 * @property {String} selected : number, default set from rowLimitOptionArray[1].
 * @property {array} facebookKeywordList: array this holds all details displayed in Facebook keyword page.
 */

app.controller("facebookKeywordController", function ($rootScope, $scope, $http, $state, Flash, $stateParams, $filter, exportToExcelService, formatdateService) {
	$scope.menu.current = "homeParent.facebookKeyword";
	var facebookKeywordList;
	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	}

	/**
	 * Get all facebook keyword list.
	 * @description - Getting facebook keyword list.		
	 * @callback requestCallback
	 * @param {requestCallback} response - The callback that handles the response.
	 * @param {list} response.data - List of facebook keywords.
	 */
	var successCallBack = function (response) {
		$scope.facebookKeywordList = response.data;
		$scope.facebookKeywordList = formatdateService.formatdate(response.data);
		$scope.exportInfo = angular.copy(response.data);
	}

	/**
	 * Get Error information.	
	 * @description - Gives error information.	
	 * @callback requestCallback
	 * @param {requestCallback} reason - The callback that handles the response.
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	}

	/**
	 * Asynchronous rest call
	 * @description - http GET request to get all facebook keywords. 
	 * @param {string} status - status of keyword (active,inactive).
	 */
	getSourceList = function (status) {
		var _url;
		if (status == 'active')
			_url = 'keyword/facebook/active';
		else if (status == 'inactive')
			_url = 'keyword/facebook/inactive';
		else
			_url = 'keyword/facebook';
		$http({
				method: "GET",
				url: _url
			})
			.then(successCallBack, errorCallBack);
	}

	/** 
	 * Reseting pop up values
	 * @description - Reset keyword details in pop-up window after adding keyword or exiting pop-up window.
	 */
	$scope.reset = function () {
		$scope.facebookKeywordType = null;
		$scope.facebookKeywordName = null;
		$scope.confirmSubmit = null;
	}

	getSourceList($stateParams.status); //on load
	$scope.orderStatus = $stateParams.status;

	/**
	 * Ordering keywords by status
	 * @description - Getting keyword list according to keyword status as active, inactive, both.
	 * @param {string} status - status of keyword(active,inactive,all)
	 */
	$scope.orderByStatus = function (status) {
		$state.go('homeParent.facebookKeyword', {
			status: $scope.orderStatus
		});
	}

	/**
	 * Addition of keyword
	 * @description - Adding keyword name and type.
	 * @param {string} _Name - Keyword name. 
	 * @param {string} _Type - Keyword type. 
	 * @param {string} orderStatus - Status of keyword.
	 */
	$scope.addingKeyword = function (_Name, _Type, orderStatus) {
		var facebookKeyword = {
			"keyword": _Name,
			"type": _Type
		};
		$http({
				method: "POST",
				url: 'keyword/facebook',
				data: facebookKeyword
			})
			.then(function (response) {
				getSourceList($stateParams.status);
				// user role implementation messages
				if ($rootScope.userRole == 'super_user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> is successfully added';
					var id = Flash.create('success', message);
				} else if ($rootScope.userRole == 'user') {
					var message = '<strong>' + _Name.toUpperCase() + '</strong> addition is pending for approval';
					var id = Flash.create('success', message);
				}

			}, function (reason) {
				var message = '<strong>Error</strong> unable to add keyword, might be due to duplicates.';
				var id = Flash.create('danger', message);
				$scope.error = reason.data;
			}); //add
		$('#facebookKeywordModal').modal('hide');
	}

	/**
	 * Changing status of keyword to Inactive.
	 * @description - Inactivating current keyword. 
	 * @param {object} _keyword - Contains single keyword details. 
	 */
	$scope.inactiveStatus = function (_keyword) {
		_keyword.disabled = true
		var facebookKeyword = {
			"keyword": _keyword.keyword,
			"type": _keyword.type
		};
		$http({
			method: "PUT",
			url: 'keyword/facebook/inactive/' + _keyword.id,
			data: facebookKeyword
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is succesfully deactivated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> deactivation is pending for approval ';
				var id = Flash.create('success', message);
			}
		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is unable to deactivate';
			var id = Flash.create('danger', message);
		});
	};

	/**
	 * Changing status of keyword to Active.
	 * @description - Activating current keyword. 
	 * @param {object} _keyword - Contains single keyword details. 
	 */
	$scope.activateStatus = function (_keyword) {
		_keyword.disabled = true
		var facebookKeyword = {
			"keyword": _keyword.keyword,
			"type": _keyword.type
		};
		$http({
			method: "PUT",
			url: 'keyword/facebook/active/' + _keyword.id,
			data: facebookKeyword
		}).then(function (response) {
			getSourceList($stateParams.status);
			// user role implementation messages
			if ($rootScope.userRole == 'super_user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is succesfully activated';
				var id = Flash.create('success', message);
			} else if ($rootScope.userRole == 'user') {
				var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> activation is pending for approval ';
				var id = Flash.create('success', message);
			}
		}, function (reason) {
			$scope.error = reason.data;
			var message = '<strong>' + _keyword.keyword.toUpperCase() + '</strong> is unable to activate';
			var id = Flash.create('danger', message);
		});
	};

	/** 
	 * Export to Excel
	 * @description - Export to Excel all facebook keywords.
	 */
	$scope.export = function () {
		var table = $scope.exportInfo;
		table = $filter('orderBy')(table, 'keyword');
		var csvString = '<table><tr><td>Keyword</td><td>Type</td><td>Created By</td><td>Last Modified At</td><td>Status</td></tr>';
		var dateInfo, statusInfo;
		for (var i = 0; i < table.length; i++) {
			var rowData = table[i];
			var d = new Date();
			d.setTime(rowData.startDate);
			dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
			if (rowData.status == true)
				statusInfo = 'Active';
			else
				statusInfo = 'Inactive';
			csvString = csvString + "<tr><td>" + rowData.keyword + "</td><td>" + rowData.type + "</td><td>" + rowData.requestedBy + "</td><td>" + dateInfo + "</td><td>" + statusInfo + "</td>";
			//	    		csvString = csvString.substring(0,csvString.length - 1);
			csvString = csvString + "</tr>";
		}
		csvString += "</table>";
		csvString = csvString.substring(0, csvString.length);
		exportToExcelService.converttoExcel(csvString, 'Facebook_Keyword');
	}

	/** 
	 * Export to pdf
	 * @description - Export to pdf all facebook keywords.
	 */
	$scope.exportpdf = function () {
		{
			var item = $scope.exportInfo;
			item = $filter('orderBy')(item, 'keyword');
			var doc = new jsPDF();
			var col = ["Keyword", "Type", "Created By", "Last Modified At", "Status"];
			var rows = [];
			var dateInfo, statusInfo;
			for (var i = 0; i < item.length; i++) {
				var rowData = item[i];
				var d = new Date();
				d.setTime(rowData.startDate);
				dateInfo = moment(d).format("MMM YYYY-MM-DD HH:mm:ss ");
				if (rowData.status == true)
					statusInfo = 'Active';
				else
					statusInfo = 'Inactive';
				var temp = [rowData.keyword, rowData.type, rowData.requestedBy, dateInfo, statusInfo];
				rows.push(temp);
			}
			doc.autoTable(col, rows);
			doc.save('Facebook_Keyword' + new Date().toDateString() + '.pdf');
		}
	};
});