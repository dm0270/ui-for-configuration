/**
 * @author Jayashri Nagpure
 * @name webApp.controller: videototextController
 * @description
 * #videototextController
 * It is a video to text controller.
 * This controller is responsible for showing the details video to text converison.
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
app.controller('videototextController', function($state,$scope,$http,$timeout, $window, Upload,$rootScope,videoToTextService) {
    /**
     * Initialisation  
     */
    $scope.initialise = function(){
    	$scope.menu.current="videototext";
        $scope.selectedSourcelanguage = 'English';
        $scope.selectedDestinationlanguage = 'Hindi';
        $scope.language = ['Hindi'];
        $scope.NewsChannel = ['CNBC TV 18'];
        $scope.selectedNewsChannel ='CNBC TV 18';
        $scope.isProcessing = false;
        $scope.isDataloaded = false;
        $scope.today = new Date().toDateString();
        $scope.issuspiciousVideo = false;
        $scope.islocalVideo = false;
        $scope.isVideoUrl = false;
    	$scope.getRumoursVideoDate();
    	assignScrollTop();
        $scope.sourceText = "";
        $scope.destinationText = "";
        $scope.lastSourceText ="";
        $scope.flag =0;
        $scope.keywords =[];
        console.log(' $scope.keywords', $scope.keywords);
        $scope.company_names =[];
        $scope.companycount =0;
        $scope.keywordcount =0;
        $('#videoSourceModal').modal('show');
    };
    
	/**
	 * Getting GPU IP details
	 * @description - REST call to Get GPU IP address.
	 */
    var getGPUIP = function(){
    	$http({
    		method:"GET",
    		url:"users/"
    	}).then(function(response){
    		$scope.gpuIP = response.data.v2t_report_address;
    	},function(reason){
    		console.log('reason'+reason);
    	})
    }
    
    /**
     * calling getGPUIP on load 
     * @description - call getGPUIP to get credentials of GPU 
     * */
    	getGPUIP();

    /**
     * Getting Text corresponding to video.
     * @description - get the text corresponding to video. 
     * */ 	
 /*   $scope.getVideoText= function(vid) {
    	$scope.isDataloaded = false;
        $scope.currentTimestamp = vid.currentTime;
        $scope.duration = vid.duration;
        if ($scope.currentTimestamp != 0) {
            if ($scope.currentTimestamp != $scope.duration) {
                if ($scope.videoMetadata[0].transcript) {
                    for (i = 0; i < $scope.videoMetadata[0].transcript.length; i++) {
                        if ($scope.videoMetadata[0].transcript[i]) {
                        	$scope.$apply(function(){
                            if ($scope.currentTimestamp >= $scope.videoMetadata[0].transcript[i].time) {
                                    $scope.sourceText = " ";
                                    $scope.destinationText = " ";
                                for (j = 0; j <= i; j++) {   
                                        $scope.sourceText = $scope.sourceText + " " + $scope.videoMetadata[0].transcript[j].sourceText;
                                        $scope.destinationText = $scope.destinationText + " " + $scope.videoMetadata[0].transcript[j].targetText;
                                        for(k=0;k<$scope.videoMetadata[0].keywords.length;k++){
                                       	 $scope.keywords = $scope.keywords+ " " + $scope.videoMetadata[0].keywords[k];
                                       }
                               	    for(l=0;l<$scope.videoMetadata[0].company_names.length;l++){
                               		     $scope.company_names =   $scope.company_names+ " " +$scope.videoMetadata[0].company_names[l];
                               	    }
                                }
                                sourceDiv.scrollTop = sourceDiv.scrollHeight;
                                destDiv.scrollTop = destDiv.scrollHeight;
                            }
                        	});
                        }
                    }
                }
            }
        }
    }
*/
    
 /*   $scope.getVideoText= function(vid) {
    	$scope.isDataloaded = false;
        $scope.currentTimestamp = vid.currentTime;
        $scope.duration = vid.duration;
        $scope.sourceText = " "; $scope.destinationText = " ";
       
        if ($scope.currentTimestamp != 0 && $scope.currentTimestamp != $scope.duration && $scope.videoMetadata[0].transcript) {
                    for (i = 0; i < $scope.videoMetadata[0].transcript.length; i++) {
                    	//$scope.sourceText = " "; $scope.destinationText = " ";
                    	
                        if ($scope.videoMetadata[0].transcript[i] && $scope.currentTimestamp >= $scope.videoMetadata[0].transcript[i].time) {
	                            	$scope.$apply(function(){
	                                   // $scope.sourceText = " ";
	                                   // $scope.destinationText = " ";
	                             //   for (j = 0; j <= i; j++) {
	                            		 	if($scope.flag==1) 
	                            		 		$scope.sourceText = $scope.lastSourceText + " " + $scope.sourceText + " " + $scope.videoMetadata[0].transcript[i].sourceText;
	                            		 	else{
	                            		 		$scope.sourceText = $scope.sourceText + " " + $scope.videoMetadata[0].transcript[i].sourceText;
		                                        $scope.destinationText = $scope.destinationText + " " + $scope.videoMetadata[0].transcript[i].targetText;
	                            		 	}
	                              //  }
	                            	 $scope.sourceDiv.scrollTop =  $scope.sourceDiv.scrollHeight;
	                            	 $scope.destDiv.scrollTop =  $scope.destDiv.scrollHeight;
	                            	});
                        }
                    }
                    if(Math.round($scope.currentTimestamp)== Math.round($scope.duration)){
                    	$scope.lastSourceText = $scope.sourceText;
                    	$scope.flag =1;
                    	console.log('last source text', $scope.lastSourceText);
                    }    
        }

    }
*/
    $scope.completeSourceText ="";$scope.completeDestinationText="";
    $scope.getVideoText= function(vid) {
    	$scope.isDataloaded = false;
        $scope.currentTimestamp = vid.currentTime;
        $scope.duration = vid.duration;
        //printing company name and keyword initialy only once 
        if($scope.currentTimestamp == 0){
        	if($scope.videoMetadata[0].company_names[0]){
        		for(i=0;i<$scope.videoMetadata[0].company_names.length;i++){
        			 $scope.company_names[$scope.companycount] = $scope.videoMetadata[0].company_names[i];
        			 $scope.companycount++;
        	         $scope.companyDiv.scrollTop =  $scope.companyDiv.scrollHeight;
        		}
        	}
        	if($scope.videoMetadata[0].keywords[0]){
        		for(i=0;i<$scope.videoMetadata[0].keywords.length;i++){	
        			$scope.keywords[$scope.keywordcount] =$scope.videoMetadata[0].keywords[i];
        			$scope.keywordcount++;
        			$scope.keywordDiv.scrollTop =  $scope.keywordDiv.scrollHeight;
        		}
        	}
        }
        if($scope.flag !=1){
            $scope.sourceText = " "; $scope.destinationText = " ";
        }else if($scope.isVideoUrl == true && $scope.flag ==1){
        	 $scope.sourceText = $scope.lastSourceText;
        	 $scope.destinationText=$scope.lastDestinationText;
        }
        if($scope.currentTimestamp != 0 && $scope.currentTimestamp != $scope.duration && $scope.videoMetadata[0].transcript) {
            for (i = 0; i < $scope.videoMetadata[0].transcript.length; i++) { 
            	if($scope.videoMetadata[0].transcript[i] && $scope.currentTimestamp >= $scope.videoMetadata[0].transcript[i].time) { 
                	$scope.$apply(function(){
     		 		 $scope.sourceText = $scope.sourceText + " " + $scope.videoMetadata[0].transcript[i].sourceText;
                     $scope.destinationText = $scope.destinationText + " " + $scope.videoMetadata[0].transcript[i].targetText;
                 	 $scope.sourceDiv.scrollTop =  $scope.sourceDiv.scrollHeight;
                 	 $scope.destDiv.scrollTop =  $scope.destDiv.scrollHeight;
                 	});
                }
            }
        }
        if($scope.isVideoUrl == true && $scope.duration==$scope.currentTimestamp){
        	for (i=0;i<$scope.videoMetadata[0].transcript.length;i++){
        		$scope.completeSourceText = $scope.completeSourceText+" "+ $scope.videoMetadata[0].transcript[i].sourceText;
        		$scope.completeDestinationText = $scope.completeDestinationText + " "+ $scope.videoMetadata[0].transcript[i].targetText;
        	}
            	$scope.lastSourceText = $scope.completeSourceText;
            	$scope.lastDestinationText = $scope.completeDestinationText;
            	$scope.flag =1;
        }   
    }

    /**
     * To get video and timeupdation in video.
     * @param {id} - #myvideo
     */
   
    $scope.vid1 = document.getElementById("localVideo");
    if($scope.vid1){
    	$scope.vid1.ontimeupdate = function() {
        	if($scope.videoMetadata){
        		$scope.getVideoText($scope.vid1);
        	}
        }
    }

    $scope.vid2 = document.getElementById("suspiciousVideo");
    if($scope.vid2){
    	$scope.vid2.ontimeupdate = function() {
            	if($scope.videoMetadata ){
            		$scope.getVideoText($scope.vid2);
            	}
            }        	
    }

    /**
     * uploading video file to server.
     * @description - uploading video file to server in formdata after clicking process button.
     */
    $scope.uploadFile = function() {
    	$scope.resetData();
    	$scope.isProcessing = true;
        var formdata = new FormData();
        formdata.append('file', $scope.file1);
        // SEND THE FILES.
        $http.post(
        		'http://'+$scope.gpuIP.trim()+':5000'+'/process', formdata, {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                }
            )
            .then(
                function(response) {
                    $scope.isProcessing = false; 
                    $scope.issuspiciousVideo = false;
                    $scope.islocalVideo = true;
                    $scope.isVideoUrl = false;
       			 	setVideoMetadata(response.data);
       			 	$scope.downloadTextFile(response.data);
                },
                function(reason) {
                	$scope.isProcessing = false; 
                    console.log('reason', reason);
                }
            );
    };
    
    /**
     * Reseting source text , responseList and destination text
     * @description - reseting values after click on process or choose file.
     */
    $scope.resetData = function(){
    	 $scope.sourceText = " ";
    	 $scope.destinationText = " ";
    	 $scope.responseList = " ";
    	 $scope.company_names = [];
    	 $scope.keywords = [];
    } 

	  $scope.invalidFiles = [];
	  // make invalidFiles array for not multiple to be able to be used in ng-repeat in the ui
	  $scope.$watch('invalidFiles', function (invalidFiles) {
	    if (invalidFiles != null && !angular.isArray(invalidFiles)) {
	      $timeout(function () {$scope.invalidFiles = [invalidFiles];});
	    }
	  });

	  $scope.$watch('files', function (files) {
	    if (files != null) {
	      // make files array for not multiple to be able to be used in ng-repeat in the ui
	      if (!angular.isArray(files)) {
	        $timeout(function () {
	          $scope.files = files = [files];
	          $scope.file1=files[0];
	          $scope.videofileName = files[0].name.split(".")[0];
	        });
	        return;
	      }
	    }
	  });


      /**
       * sending live streaming url to bckend and it will start video chunks downloading process.   
       */
      $scope.sendLiveUrl = function(){
    	$scope.isProcessing = true;   
    	var fd = new FormData();
    	//fd.append('url', $scope.youtubeVideoUrl);
    	fd.append('url', $scope.selectedNewsChannel);
        $http.post(
        		'http://'+$scope.gpuIP.trim()+':5004/get_chunk',fd,{                   
        			transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
        		}
            )
            .then(
                function(response) {
                	console.log('url sent succesfully',response.data);
                	setVideoMetadata(response.data);
                },
                function(reason) {
                	$scope.isProcessing = false; 
                    console.log('reason', reason);
                }
            );
      };
      
      /**
       *  $scope.restartProcess();
       */
      $scope.restartProcess = function(){
      	$http({
    		method:"POST",
    		url:"http://"+$scope.gpuIP.trim()+":5003/restart_api"
    	}).then(function(response){
    	},function(reason){
    		console.log('reason'+reason);
    	})    	  
      };
      
      /**
       * Terminate live streaming download functionality and clearing source text ,destination text, along with video 
       */
      $scope.terminateLiveUrl = function(){
    	$http({
    		method:"POST",
    		url:"http://"+$scope.gpuIP.trim()+":5003/terminate_download"
    	}).then(function(response){
    	  $scope.videoMetadata =" ";
    	  $scope.video_path = "";
    	  $scope.sourceText =" ";
    	  $scope.destinationText =" ";
	      $scope.company_names = [];
	      $scope.keywords = [];
    	  $scope.lastSourceText = " ";
    	  $scope.lastDestinationText =" ";
		  $scope.youtubeVideoUrl = " ";
	      $scope.restartProcess();
    	},function(reason){
    		console.log('reason'+reason);
    	})    	  
      };
            
      /**
       * Terminating process.
       */
      $scope.terminateProcess = function(){
    	 // websocket.close();
    	  $scope.terminateLiveUrl();
      };

	  /**
	   * Download to text file.
	   * @description - download text file for source and destination text.
	   */
	 
	  $scope.downloadTextFile = function(_downloadInfo){
		  var sourceData="",destinationData="";
		  // videofilename for download file
		  $scope.videofileName = _downloadInfo[0].video_name.split(".")[0];
		 for(i=0;i<_downloadInfo[0].transcript.length;i++){
			  sourceData = sourceData +" "+ _downloadInfo[0].transcript[i].sourceText;
			  destinationData = destinationData +" "+ _downloadInfo[0].transcript[i].targetText;
		 }
        sourceblob = new Blob([sourceData], { type: 'text/plain' }),
        sourceUrl = $window.URL || $window.webkitURL;
	    $scope.sourceFileUrl = sourceUrl.createObjectURL(sourceblob);
        destinationblob = new Blob([destinationData], { type: 'text/plain' }),
        destinationUrl = $window.URL || $window.webkitURL;
	    $scope.destinationFileUrl = destinationUrl.createObjectURL(destinationblob);
	  };
	  
	  /** 
	   * Reseting pop up values
	   * @description - Reset keyword details in pop-up window after adding keyword or exiting pop-up window.
	  */
	$scope.reset = function(){
		$scope.video_source = "";
		$scope.sourceText ="";
		$scope.destinationText ="";
		$scope.video_path = "";
		$scope.videoMetadata ="";
		if($scope.files)
		$scope.files[0]=" ";
		$scope.company_names =[];
		$scope.keywords = [];
	};

	/**
	 * API to get next chunk in Live video URL 
	 */
	function getYoutubeLiveMetadata(){
		videoToTextService.getMetadata()
		 .then(function(response) {
			 $scope.isProcessing = false;
			 setVideoMetadata(response.data);
		 },
		 function(data) {
		  $scope.isProcessing = false;
		  console.log('Failed to load Data',data);
		 });
	};
	
	/**
	 * Handling video Ended Event,calling next chunk after ending of first chunk
	 */
	    document.getElementById('suspiciousVideo').addEventListener('ended', nextChunkHandler, false);
		function nextChunkHandler() {
			if($scope.isVideoUrl){
				console.log('end video');
				$scope.isProcessing = true;
				getYoutubeLiveMetadata();
			}
		};
	
	/**
	 * Adding video source 
	 * @description - Added video source
	 */
 
	$scope.addingVideoSource = function(video_Source){
		if(video_Source =='video_url'){	
			console.log(' $scope.keywords', $scope.keywords);
			$scope.isVideoUrl = true;
			$scope.issuspiciousVideo = false;
			$scope.islocalVideo = false;
			$scope.isProcessing = false;
			$('#videoSourceModal').modal('hide');
		}else if(video_Source =='rumours_video'){
			$scope.issuspiciousVideo = true;
			$scope.islocalVideo = false;
			$scope.isVideoUrl = false;
			$scope.isProcessing = false;
			$state.go("homeParent.rumoursVideo");
		}else if(video_Source =='local_video'){	
			$scope.islocalVideo = true;
			$scope.isProcessing = false;
			$scope.issuspiciousVideo = false;
			$scope.isVideoUrl = false;
			$('#videoSourceModal').modal('hide');
		}
	};
		
	/**
	 * Setting metadata for multiple options. 
	 * @description - setting metadata from diffrent inputs as local video metadata or rumours metadat.
	 */
	function setVideoMetadata(_response){
		$scope.videoMetadata = _response;
		if($scope.isVideoUrl){
			$scope.isProcessing = false; 
			$scope.video_path = "http://"+$scope.gpuIP.trim()+"/video/"+_response[0].video_name;
			console.log('_response',_response);
		}else if($scope.issuspiciousVideo){
			console.log(_response);
			$scope.video_path ="http://"+$scope.gpuIP.trim()+"/suspicious_videos/"+_response[0].video_name;
		}
	}
	
	/**
	 * getRumoursVideoMetadata
	 * @description -
	 */
	getRumoursVideoMetadata = function(video_date){
		videoToTextService.getRumoursMetadata(video_date)
		 .then(function(response) {
			 $scope.islocalVideo = false;
			 $scope.issuspiciousVideo = true;
			 $scope.isVideoUrl = false;
			 setVideoMetadata(response.data);
			 $scope.downloadTextFile(response.data);
		 },
		 function(data) {
		  console.log('Failed to load Data',data);
		 });
	}
	/**
	 * acceesing rumpurs video date to get corresponding video data
	 * @decription - accessing selected rumours video date to get corresponding video metadata. 
	 */
	$scope.getRumoursVideoDate=function(){
		if($rootScope.selectedRumoursVideoDate){
			getRumoursVideoMetadata($rootScope.selectedRumoursVideoDate); 
		}
	}
	
    /**
     * Assigning scroll bar's top with the height of div
     * @description - 
     */
    function assignScrollTop () {
         $scope.sourceDiv = document.getElementById("sourceTextDiv");
         $scope.destDiv = document.getElementById("destinationTextDiv");
         $scope.companyDiv = document.getElementById("companyNameDiv");
         $scope.keywordDiv = document.getElementById("rumoursKeywordDiv");
         $scope.sourceDiv.scrollTop =  $scope.sourceDiv.scrollHeight;
         $scope.destDiv.scrollTop =  $scope.destDiv.scrollHeight;
         $scope.companyDiv.scrollTop =  $scope.companyDiv.scrollHeight;
         $scope.keywordDiv.scrollTop =  $scope.keywordDiv.scrollHeight;
    }
    
	/**
	 * Onload functions
	 * @description - calling function onload
	 */
	$scope.initialise();
});
