/**
 * @author Jayashri Nagpure
 * @name webApp.controller: rumoursVideoController
 * @description
 * It is rumoursVideoController which will handle already proccessed rumours video for its source text and translation.
 * This controller is responsible for showing the details video to text converison.
 * @requires $state
 * @requires $rootScope
 * @requires $scope
 * @requires $http
 */
app.controller('rumoursVideoController', function($state, $scope,$rootScope,videoToTextService) {
	// on load Date should be blank to get all data
	$scope.startDate = '';
	$scope.endDate = '';
	$scope.rowLimitOptionArray = [10,20,30,50,100];  
	$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]};
	
	
	/**
	 * Getting rumours videos from hdfs
	 * @description - Getting list of rumours videos
	 */
	 $scope.getRumoursVideoList = function(){
		 videoToTextService.getVideoList()
		 .then(function(response) {
			 $scope.videoList = response.data;
		 },
		 function(data) {
		  console.log('Failed to load Data',data);
		 });
	}
	 
	/**
	 * Onclick of view button.
	 * @description- 
	 */
	 $scope.getVideoAndDisplayRumour = function(folder_path){
		 $rootScope.selectedRumoursVideoDate = folder_path;;
	 }
	 
	 /**
	  * Onload functions
	  * @description - calling function onload
	  */
	 $scope.getRumoursVideoList();

});