app.controller('rejectedRequestController',function($state,Flash,$scope,$http,$filter,exportToExcelService,sortDataService) {
		$scope.menu.current="rejected";
		
		//sorting column 
		$scope.sortColumn = "id";
		$scope.reverseSort = false;

		$scope.rowLimitOptionArray = [10,20,30,50,100];  
		$scope.selected = {rowLimit:$scope.rowLimitOptionArray[1]};

	/**
	 * Sets rejected request list 
	 * response:successful rest call response contains rejected request data
	 */
	var successCallBack = function (response) {
		$scope.allRequestList = response.data;
		$scope.allRequestList.forEach(function (element) {
			if (element.requestType.requestMedia == 'all_english' || element.requestType.requestMedia == 'all_hindi') {
				element.companyrequestedValue = element.requestedValue.substring(element.requestedValue.indexOf(':') + 1, element.requestedValue.length);
			}
			if (element.requestedOperation == 'ADD') {
				element.addexistingValue = '';
			}
		})

		$scope.exportInfo = angular.copy(response.data);
	};

	/**
	 * Sets error 
	 * reason:error information
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	};

	/**
	 * Get reject request data 
	 * status:status of request(all,pending,approved,rejected)
	 */
	getAllRequestList = function (status) {
		var _url;
		if (status == 'rejected')
			_url = 'request/rejected';
		$http({
				method: "GET",
				url: _url
			})
			.then(successCallBack, errorCallBack);
	};


	//On load 
	getAllRequestList('rejected');

	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}

	/**
	 * Exports to Excel
	 */
	$scope.export = function () {
		var table = $scope.exportInfo;
		table = $filter('orderBy')(table, 'id');
		var csvString = '<table><tr><td>Request Id</td><td>Requested By</td><td>Requested Operation</td><td>Existing Value</td><td>Requested Value</td><td>Media Type</td><td>Category</td><td>Rejecters Comment</td><td>Rejected By</td></tr>';
		for (var i = 0; i < table.length; i++) {
			var rowData = table[i];
			if (rowData.requestedOperation == 'ADD') {
				rowData.existingValue = rowData.addexistingValue;
			}
			if (rowData.requestType.requestMedia == 'all_english' || rowData.requestType.requestMedia == 'all_hindi') {
				rowData.requestedValue = rowData.companyrequestedValue
			}

			csvString = csvString + "<tr><td>" + rowData.id + "</td><td>" + rowData.requestedBy + "</td><td>" +
				rowData.requestedOperation + "</td><td>" + rowData.existingValue + "</td><td>" + rowData.requestedValue + "</td><td>" + rowData.requestType.requestType + "</td><td>" + rowData.requestType.requestMedia + "</td><td>" + rowData.comment + "</td><td>" + rowData.actionedBy + "</td>";
			csvString = csvString + "</tr>";
		}

		csvString += "</table>";
     	csvString = csvString.substring(0, csvString.length);
     	exportToExcelService.converttoExcel(csvString,'Rejected_Requests');	 
	}

	/**
	 * Exports to pdf
	 */
	$scope.exportpdf = function () {
		{
			var item = $scope.exportInfo;
			item = $filter('orderBy')(item, 'id');
			var doc = new jsPDF('l','cm',[30,30]);
			var col = ["Request Id", "Requested By", "Requested Operation", "Existing Value", "Requested Value", "Media Type", "Category", "Rejecters Comment", "Rejected By"];
			var rows = [];
			var dateInfo, statusInfo;
			for (var i = 0; i < item.length; i++) {
				var rowData = item[i];
				if (rowData.requestedOperation == 'ADD') {
					rowData.existingValue = rowData.addexistingValue;
				}
				if (rowData.requestType.requestMedia == 'all') {
					rowData.requestedValue = rowData.companyrequestedValue
				}
				var temp = [rowData.id, rowData.requestedBy, rowData.requestedOperation, rowData.existingValue, rowData.requestedValue, rowData.requestType.requestType, rowData.requestType.requestMedia, rowData.comment, rowData.actionedBy];
				rows.push(temp);
			}
			doc.autoTable(col, rows);
			doc.save('Rejected_Requests' + new Date().toDateString() + '.pdf');
		}
	};

});