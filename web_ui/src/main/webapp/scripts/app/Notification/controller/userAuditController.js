app.controller('userAuditController',function($rootScope,$state,Flash,$scope,$http,$filter,exportToExcelService,formatdateService,sortDataService) {
	
	//sorting column 
	$scope.sortColumn = "id";
	$scope.reverseSort = false;

	$scope.rowLimitOptionArray = [10, 20, 30, 50, 100];
	$scope.selected = {
		rowLimit: $scope.rowLimitOptionArray[1]
	};

	/**
	 * Sets audit list 
	 * response:successful rest call response contains audit data
	 */
	var successCallBack = function (response) {
		$scope.allRequestList = response.data;
		$scope.allRequestList = formatdateService.formatdate(response.data);
		$scope.allRequestList.forEach(function (element) {
			if (element.requestType.requestMedia == 'all_english' || element.requestType.requestMedia == 'all_hindi') {
				element.companyrequestedValue = element.requestedValue.substring(element.requestedValue.indexOf(':') + 1, element.requestedValue.length);
			}
			if (element.requestedOperation == 'ADD') {
				element.addexistingValue = '';
			}
		})
		$scope.exportInfo = angular.copy(response.data);
	};

	/**
	 * Sets error 
	 * reason:error information
	 */
	var errorCallBack = function (reason) {
		$scope.error = reason.data;
	};

	// on load Date should be blank to get all data
	//$scope.startDate = $filter('date')(new Date(), 'EEE-yyyy-MM-dd');
	//$scope.endDate = $filter('date')(new Date(), 'EEE-yyyy-MM-dd');
	$scope.startDate ="";
	$scope.endDate="";
	/**
	 * Get audit data 
	 * status:status of request(all,pending,approved,rejected)
	 */
	$scope.getAllRequestList = function (status) {
		var _url;
		if (status == 'all')
			_url = 'request';
		$http({
				method: "GET",
				url: _url
			})
			.then(successCallBack, errorCallBack);
	};

	//$scope.orderStatus=$stateParams.status;

	$scope.getAllRequestList('all'); // on load 

	//sorting column data after click on column name
	$scope.sortData = function (column) {
		sortDataService.sortData(column,$scope.sortColumn,$scope.reverseSort);
		$scope.reverseSort = sortDataService.sortDataServiceObject.reverseSort;
		$scope.sortColumn = sortDataService.sortDataServiceObject.sortColumn;
	}

	//getting column name to sort data
	$scope.getSortClass = function (column) {
		return sortDataService.getSortClass(column,$scope.sortColumn,$scope.reverseSort);
	}

	/**
	 * Exports to Excel
	 */
    $scope.export = function(){
    	var table = $scope.exportInfo;
    	 table = $filter('orderBy')(table, 'id');
        	var csvString = '<table><tr><td>Request Id</td>'+
        		'<td>Requested By</td>'+
        		'<td>Requested Date</td>'+
        		'<td>Approved By</td>'+
        		'<td>Approved Date</td>'+
        		'<td>Existing Value</td>'+
        		'<td>Requested Value</td>'+
        		'<td>Requested Operation</td>'+
        		'<td>Requested Type</td>'+
        		'<td>Requested Media</td>'+
        		'<td>Status</td></tr>';
          	var requestedDate, actionedDate;
        	for(var i=0; i<table.length;i++){
        		var rowData = table[i];
        		if(rowData.requestedOperation == 'ADD'){
        			rowData.existingValue = rowData.addexistingValue;
        		}
        		if(rowData.requestType.requestMedia == 'all_english' || rowData.requestType.requestMedia == 'all_hindi'){
        			rowData.requestedValue = rowData.companyrequestedValue
        		}	    
		   		if(rowData.actionedBy == null ){
        			rowData.actionedBy = '';
    	    	}
        		if(rowData.actionedDate == null ){
        			rowData.actionedDate = '';
        		}
	 		
        		csvString = csvString + "<tr><td>" +rowData.id + "</td><td>" + rowData.requestedBy+"</td><td>" + rowData.requestedDate+"</td><td>" 
        		+ rowData.actionedBy+"</td><td>" + rowData.actionedDate+"</td><td>" + rowData.existingValue+"</td><td>" + rowData.requestedValue+"</td><td>"
        		+ rowData.requestedOperation+"</td><td>" + rowData.requestType.requestType+"</td><td>" + rowData.requestType.requestMedia+"</td><td>"+ rowData.requestStatus+"</td>";
        		csvString = csvString + "</tr>";
    	    } 
        	
    	csvString += "</table>";
     	csvString = csvString.substring(0, csvString.length);
     	exportToExcelService.converttoExcel(csvString,'User_Audit');	        
    }


	/**
	 * Exports to pdf
	 */
    $scope.exportpdf = function () {
    	{
    		var item = $scope.exportInfo;
    		item = $filter('orderBy')(item, 'id');
    	    var doc = new jsPDF('landscape');

    	    var col = ["Request Id","Requested By","Requested Date","Approved By","Approved Date","Existing Value","Requested Value","Requested Operation","Requested Type","Requested Media","Status"];
     	    var rows = [];
          	var requestedDate, actionedDate;    	    
    	    for(var i=0; i<item.length;i++){
        		var rowData = item[i];
        		if(rowData.requestedOperation == 'ADD'){
        			rowData.existingValue = rowData.addexistingValue;
        		}
        		if(rowData.requestType.requestMedia == 'all_english' || rowData.requestType.requestMedia == 'all_hindi'){
        			rowData.requestedValue = rowData.companyrequestedValue
        		}     
        		if(rowData.actionedBy == null ){
        			rowData.actionedBy = '';
	    	    }
        		if(rowData.actionedDate == null ){
        			rowData.actionedDate = '';
        		}  		
            	var	temp = [rowData.id,rowData.requestedBy,rowData.requestedDate,rowData.actionedBy,rowData.actionedDate,rowData.existingValue, rowData.requestedValue,
            		rowData.requestedOperation,rowData.requestType.requestType,rowData.requestType.requestMedia,rowData.requestStatus];
        		rows.push(temp);
    	    }

    	   doc.autoTable(col, rows);

    	   doc.save('User_Audit '+ new Date().toDateString() +'.pdf');
    	  }
    };


});