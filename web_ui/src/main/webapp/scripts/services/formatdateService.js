/**
 * @author Jayashri Nagpure
 * @name webApp.service: formatdateService
 * @description
 * #formatdateService
 * It is format date service.
 * This service is responsible for formatting date in required form.
 */

app.service('formatdateService', function ($filter) {

	/**
	 * Formating input date.
	 * @description - Formating input date into  'EEE-yyyy-MM-dd HH:mm:ss' format.
	 * @param {object} _List - List
	 */
	 this.formatdate = function(_List){
		_List.forEach(function(x){
			if(x.approveDate){
	    		x.approveDate = $filter('date')(x.approveDate, 'EEE-yyyy-MM-dd HH:mm:ss');
			}
			if(x.requestedDate){
	    		x.requestedDate = $filter('date')(x.requestedDate, 'EEE-yyyy-MM-dd HH:mm:ss');
			}
			if(x.actionedDate){
	    		x.actionedDate = $filter('date')(x.actionedDate, 'EEE-yyyy-MM-dd HH:mm:ss');
			}
		   })
		   return _List;
	}
});