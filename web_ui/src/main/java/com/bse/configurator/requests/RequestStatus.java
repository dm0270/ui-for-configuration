package com.bse.configurator.requests;

public enum RequestStatus {
	PENDING,
	APPROVED,
	REJECTED
}
