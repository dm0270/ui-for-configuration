package com.bse.configurator.requests;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bse.configurator.request.type.RequestType;

@Repository
public interface RequestRepository extends JpaRepository<Request, Long>{
	@Query("select t from Request t")
	List<Request> findAllRequest();

	@Query("select t from Request t where  t.id = ?1")
	Request findById(long id);
	
	@Query("select t from Request t where  t.requestStatus = ?1")
	List<Request> findRecordsByStatus(RequestStatus status);

	@Query("select t from Request t where  t.requestType=?1  and t.requestedValue=?2")
	Request doesRequestExists(RequestType requestType,String requestInfoName);

}
