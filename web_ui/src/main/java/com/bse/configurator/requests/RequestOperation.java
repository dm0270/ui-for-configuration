package com.bse.configurator.requests;

public enum RequestOperation {
	ADD,
	ACTIVE,
	INACTIVE,
	UPDATE,
	REMOVE
}
