package com.bse.configurator.keyword;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.email.Email;
import com.bse.configurator.email.EmailService;
import com.bse.configurator.exception.DuplicateFoundException;
import com.bse.configurator.exception.ResourceNotFoundException;
import com.bse.configurator.request.type.RequestType;
import com.bse.configurator.request.type.RequestTypeRepository;
import com.bse.configurator.requests.Request;
import com.bse.configurator.requests.RequestOperation;
import com.bse.configurator.requests.RequestRepository;
import com.bse.configurator.util.Constants;

@RestController
@RequestMapping("/keyword")
public class KeywordController {
	private KeywordRepository keywordRepository;
	private RequestRepository requestRepository;
	private RequestTypeRepository requestTypeRepository;
	private Email email;
	@Autowired
	EmailService emailService;

	@Autowired
	public KeywordController(KeywordRepository keywordRepository, RequestRepository requestRepository,
			RequestTypeRepository requestTypeRepository) {
		this.keywordRepository = keywordRepository;
		this.requestRepository = requestRepository;
		this.requestTypeRepository = requestTypeRepository;
		email = new Email();
	}

	private KeywordMedia getKeywordMedia(String media) {
		switch (media.toLowerCase()) {
		case "web_english":
			return KeywordMedia.WEB_ENGLISH;
		case "web_hindi":
			return KeywordMedia.WEB_HINDI;
		case "twitter":
			return KeywordMedia.TWITTER;
		case "facebook":
			return KeywordMedia.FACEBOOK;
		}
		return null;
	}

	public void saveKeyword(Keyword keyword, String approvedBy, String createdBy) {
		keyword.setApprovedBy(approvedBy);
		keyword.setRequestedBy(createdBy);
		keywordRepository.save(keyword);
	}

	public void saveKeyword(Keyword keyword, String createdBy) {
		keyword.setApprovedBy(createdBy);
		keyword.setRequestedBy(createdBy);
		keywordRepository.save(keyword);
	}

	@GetMapping("/{media:web_english|web_hindi|twitter|facebook}")
	public List<Keyword> getSources(@PathVariable("media") String media) {
		KeywordMedia keywordMedia = this.getKeywordMedia(media);
		return this.keywordRepository.findAllRecords(keywordMedia.toString());
	}

	@PostMapping("/{media:web_english|web_hindi|twitter|facebook}")
	public void addKeyword(@Valid @RequestBody Keyword keyword, @PathVariable("media") String media) {
		boolean authorized = false;
		boolean isCreatorSuperUser = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		KeywordMedia keywordMedia = this.getKeywordMedia(media);
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (authorized) {
			if (this.keywordRepository.doesKeywordExists(keyword.getKeyword(), keywordMedia) != null) {
				throw new DuplicateFoundException((long) 400, "Duplicate entry found for keyword.");
			} else {
				keyword.setMedia(keywordMedia);
				if (keyword.getRequestedBy() == null) {
					keyword.setRequestedBy(username);
				}
				if (keyword.getRequestedDate() == null) {
					keyword.setRequestedDate(new Date());
					keyword.setApproveDate(keyword.getStartDate());
					isCreatorSuperUser = true;
				}
				this.saveKeyword(keyword, username, keyword.getRequestedBy());
				if(isCreatorSuperUser == true) {
				String requestTypeKey = keyword.getType().toString().toLowerCase();
				RequestType requestType = this.requestTypeRepository.findBySourceType(requestTypeKey, media);
				Request request = new Request(requestType, keyword.getKeyword(),keyword.getKeyword(), username, username,new Date(),new Date(),
						RequestOperation.ADD);
				this.requestRepository.save(request);
				}
				this.email.setMessage(String.format("%s has added '%s' %s %s keyword at %s.", username,
						keyword.getKeyword(), keywordMedia, keyword.getType(), keyword.getStartDate()));
				this.emailService.send(this.email);
			}
		} else {
			if (this.keywordRepository.doesKeywordExists(keyword.getKeyword(), keywordMedia) != null) {
				throw new DuplicateFoundException((long) 400, "Duplicate entry found for keyword.");
			} else {
				String requestTypeKey = keyword.getType().toString().toLowerCase();
				String requestedValue = keyword.getKeyword();
				RequestType requestType = this.requestTypeRepository.findBySourceType(requestTypeKey, media);
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, username, requestedDate,
						RequestOperation.ADD);// for other user
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to add '%s' %s %s keyword at %s.", username,
						keyword.getKeyword(), keywordMedia, keyword.getType(), keyword.getStartDate()));
				this.emailService.send(this.email);
			}
		}
	}

	@GetMapping("/{media:web_english|web_hindi|twitter|facebook}/{status:active|inactive}")
	public List<Keyword> getKeywords(@PathVariable("media") String media, @PathVariable("status") String status) {
		KeywordMedia keywordMedia = this.getKeywordMedia(media);
		switch (status) {
		case "active":
			return this.keywordRepository.findRecordsByStatus(true, keywordMedia.toString());
		case "inactive":
			return this.keywordRepository.findRecordsByStatus(false, keywordMedia.toString());
		}
		return null;
	}

	@PutMapping("/{media:web_english|web_hindi|twitter|facebook}/inactive/{id}")
	public void deActivateKeyword(@Valid @RequestBody Keyword keyword, @PathVariable("id") Long id,
			@PathVariable("media") String media) {
		boolean authorized = false;		
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		KeywordMedia keywordMedia = this.getKeywordMedia(media);
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Keyword keywordFound = this.keywordRepository.findByActive(id, keyword.getType(), keywordMedia,
				keyword.getKeyword());
		if (authorized) {
			if (keywordFound == null) {
				throw new ResourceNotFoundException((long) 400, "Keyword not found.");
			} else {
				keywordFound.setMedia(keywordMedia);
				keywordFound.setEndDate(new Date());
				this.keywordRepository.save(keywordFound);
				Keyword updatedKeyword = new Keyword(keywordFound.getKeyword(), keywordFound.getType(),
						keywordFound.getMedia(), false);
				updatedKeyword.setMedia(keywordMedia);
				updatedKeyword.setRequestedBy(username);
				if (keyword.getRequestedDate() == null) {
				updatedKeyword.setRequestedDate(new Date());
				updatedKeyword.setApproveDate(keyword.getStartDate());
				String requestTypeKey = keyword.getType().toString().toLowerCase();
				RequestType requestType = this.requestTypeRepository.findBySourceType(requestTypeKey, media);
				Request request = new Request(requestType, keyword.getKeyword(),keyword.getKeyword(), username, username,new Date(),new Date(),
						RequestOperation.INACTIVE);
				this.requestRepository.save(request);
				}
				this.saveKeyword(updatedKeyword, username);
				this.email.setMessage(String.format("%s has deactivated '%s' %s %s keyword at %s.", username,
						keyword.getKeyword(), keywordMedia, keyword.getType(), keyword.getStartDate()));
				this.emailService.send(this.email);
			}
		} else {
			if (keywordFound == null) {
				throw new ResourceNotFoundException((long) 400, "Keyword not found.");
			} else {
				String requestTypeKey = keyword.getType().toString().toLowerCase();
				String requestedValue = keyword.getKeyword();
				RequestType requestType = this.requestTypeRepository.findBySourceType(requestTypeKey, media);
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, username, requestedDate,
						RequestOperation.INACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to deactivate '%s' %s %s keyword at %s.", username,
						keyword.getKeyword(), keywordMedia, keyword.getType(), keyword.getStartDate()));
				this.emailService.send(this.email);
			}
		}
	}

	public void approveKeywordRequest(Keyword existingKeyword, Keyword newKeyword, String approvedBy,
			String createdBy) {

		if (existingKeyword == null) {
			throw new ResourceNotFoundException((long) 400, "Keyword not found.");
		} else {
			existingKeyword.setEndDate(new Date());
			this.keywordRepository.save(existingKeyword);
			newKeyword.setMedia(existingKeyword.getMedia());
			this.saveKeyword(newKeyword, approvedBy, createdBy);
			this.emailService.send(this.email);
		}
	}

	@PutMapping("/{media:web_english|web_hindi|twitter|facebook}/active/{id}")
	public void ActivateKeyword(@Valid @RequestBody Keyword keyword, @PathVariable("id") Long id,
			@PathVariable("media") String media) {
		boolean authorized = false;
		authorized = SecurityContextHolder.getContext().getAuthentication().getAuthorities()
				.contains(new SimpleGrantedAuthority(Constants.SUPERUSER));
		KeywordMedia keywordMedia = this.getKeywordMedia(media);
		String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Keyword keywordFound = this.keywordRepository.findByInactive(id, keyword.getType(), keywordMedia,
				keyword.getKeyword());
		if (authorized) {
			if (keywordFound == null) {
				throw new ResourceNotFoundException((long) 400, "Keyword not found.");
			} else {
				keywordFound.setMedia(keywordMedia);
				keywordFound.setEndDate(new Date());
				this.keywordRepository.save(keywordFound);
				Keyword updatedKeyword = new Keyword(keywordFound.getKeyword(), keywordFound.getType(),
						keywordFound.getMedia(), true);
				updatedKeyword.setMedia(keywordMedia);
				updatedKeyword.setRequestedBy(username);
				updatedKeyword.setRequestedDate(new Date());
				updatedKeyword.setApproveDate(keyword.getStartDate());
				this.saveKeyword(updatedKeyword, username);
				String requestTypeKey = keyword.getType().toString().toLowerCase();
				RequestType requestType = this.requestTypeRepository.findBySourceType(requestTypeKey, media);
				Request request = new Request(requestType, keyword.getKeyword(),keyword.getKeyword(), username, username,new Date(),new Date(),
						RequestOperation.ACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has activated '%s' %s %s keyword at %s.", username,
						keyword.getKeyword(), keywordMedia, keyword.getType(), keyword.getStartDate()));
				this.emailService.send(this.email);
			}
		} else {
			if (keywordFound == null) {
				throw new ResourceNotFoundException((long) 400, "Keyword not found.");
			} else {
				String requestTypeKey = keyword.getType().toString().toLowerCase();
				String requestedValue = keyword.getKeyword();
				RequestType requestType = this.requestTypeRepository.findBySourceType(requestTypeKey, media);
				Date requestedDate = new Date();
				Request request = new Request(requestType, requestedValue, username, requestedDate,
						RequestOperation.ACTIVE);
				this.requestRepository.save(request);
				this.email.setMessage(String.format("%s has requested to activate '%s' %s %s keyword at %s.", username,
						keyword.getKeyword(), keywordMedia, keyword.getType(), keyword.getStartDate()));
				this.emailService.send(this.email);
			}
		}
	}

}
