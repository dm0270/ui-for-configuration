package com.bse.configurator.exception;

public class DuplicateFoundException extends RuntimeException {
    private Long resourceId;

    public DuplicateFoundException(Long resourceId, String message) {
        super(message);
        this.resourceId = resourceId;
    }
}
