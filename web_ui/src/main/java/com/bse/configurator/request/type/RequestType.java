package com.bse.configurator.request.type;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.bse.configurator.requests.Request;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="request_type")
public class RequestType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private int id;
	private String requestType,requestMedia;
	@OneToMany(targetEntity=Request.class,mappedBy="requestType", cascade=CascadeType.ALL,fetch = FetchType.LAZY)
	private List<Request> requests;

	public RequestType(){
		
	}
	
	public RequestType(String requestType,String requestMedia) {
		this.requestType=requestType;
		this.requestMedia=requestMedia;
	}
	public RequestType(int id,String RequestType,String requestMedia) {
		this.id=id;
		this.requestType=RequestType;
		this.requestMedia=requestMedia;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRequestType() {
		return requestType;
	}
	public void setRequestType(String RequestType) {
		this.requestType = RequestType;
	}
	
	public String getRequestMedia() {
		return requestMedia;
	}
	public void setRequestMedia(String requestMedia) {
		this.requestMedia = requestMedia;
	}
	
	@JsonIgnore
	public List<Request> getRequest(){
		return this.requests;
	}
}
