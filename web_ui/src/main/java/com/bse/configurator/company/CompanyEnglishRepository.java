package com.bse.configurator.company;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface CompanyEnglishRepository extends JpaRepository<CompanyEnglish, Long>{
	@Query(nativeQuery=true,value="SELECT CASE WHEN t2.existing_value IS NULL THEN false ELSE true END as requested,t1.* from public.companies t1 LEFT OUTER JOIN (SELECT rt.request_media,rt.request_type,r.* from public.request r INNER JOIN public.request_type rt ON r.request_type_id = rt.id where rt.request_media=?1 and rt.request_type in (?2) and r.request_status='PENDING') t2 ON t1.company_grams=t2.existing_value where t1.end_date='9999-12-31 23:59:59'")
	List<CompanyEnglish> findAllRecords(String mediaType,String requestType);
	
	@Query("select t from CompanyEnglish t where t.endDate='9999-12-31 23:59:59' and t.status=true and t.id=?1")
	CompanyEnglish findByActiveId(Long id);
	
	@Query("select t from CompanyEnglish t where t.endDate='9999-12-31 23:59:59' and t.status=false and t.id=?1")
	CompanyEnglish findByInactiveId(Long id);
	
	@Query("select t from CompanyEnglish t where t.endDate='9999-12-31 23:59:59' and t.status=?1")
	List<CompanyEnglish> findByStatus(Boolean status);
	
	@Query("select t from CompanyEnglish t where t.endDate='9999-12-31 23:59:59' and t.id=?1")
	CompanyEnglish findById(Long id);
	

}
