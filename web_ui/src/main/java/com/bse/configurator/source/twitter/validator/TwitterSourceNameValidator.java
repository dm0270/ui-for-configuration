package com.bse.configurator.source.twitter.validator;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.bse.configurator.source.twitter.TwitterSource;

public class TwitterSourceNameValidator implements ConstraintValidator<TwitterSourceName, TwitterSource> {
	@Override
	public void initialize(TwitterSourceName constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(TwitterSource twitterSource, ConstraintValidatorContext context) {
		Pattern hashtagPattern = Pattern.compile("[##]+[A-Za-z]*([A-Za-z0-9-_\\[.*?\\]]+)");
		Pattern handlePattern = Pattern.compile("[@@]+[A-Za-z]*([A-Za-z0-9-_\\[.*?\\]]+)");
		System.out.println(twitterSource.getSourceTypeName());
		if (twitterSource.getSourceTypeName() == null) {
			return false;
		}

		if (twitterSource.getSourceTypeName().equals("handle") == true
				&& handlePattern.matcher(twitterSource.getSourceName()).matches() == true) {
			return true;
		} else if (twitterSource.getSourceTypeName().equals("hashtag") == true
				&& hashtagPattern.matcher(twitterSource.getSourceName()).matches() == true) {
			return true;
		} else {
			return false;
		}
	}
}
