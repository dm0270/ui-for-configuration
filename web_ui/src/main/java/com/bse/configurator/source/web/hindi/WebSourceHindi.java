package com.bse.configurator.source.web.hindi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.bse.configurator.source.SourceType;

@Entity
@Table(name = "web_source_hindi")
public class WebSourceHindi {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="web_source_id")
	private long id;
	@ManyToOne(cascade=CascadeType.MERGE)
    @JoinColumn(name = "web_source_type_id")
	private SourceType sourceType;
	@Column(name="web_source_link")
	private String link;
	@Column(name="web_source_site")
	private String site;
	private String createdBy;
	@Column(name="web_source_text")
	private String text;
	@Column(name="web_source_title")
	private String title;
	@Column(name="web_source_html_url")
	private String htmlUrl;
	private Date startDate=new Date();
	private Date endDate = new Date();
	@Column(name="web_source_status")
	private boolean status = true;
	
	public WebSourceHindi() {
		try {
			this.endDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse("9999-12-31 23:59:59");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getHtmlUrl() {
		return htmlUrl;
	}

	public void setHtmlUrl(String htmlUrl) {
		this.htmlUrl = htmlUrl;
	}

	public SourceType getSourceType() {
		return sourceType;
	}

	public WebSourceHindi(String link,SourceType sourceType) {
		this.link = link;
		this.sourceType=sourceType;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}


	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = new Date();
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = new Date();
	}
	
	
    public SourceType getSourceTypes() {
        return this.sourceType;
    }
	
	public void setSourceType(SourceType sourceType) {
		this.sourceType=sourceType;
	}
}
