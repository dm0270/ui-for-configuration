package com.bse.configurator.source;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
@Repository
public interface SourceRepository extends JpaRepository<SourceType, Long>{
	@Query("select s from SourceType s where s.sourceType=?1 and s.sourceMedia=?2")
	SourceType findBySourceType(String sourceType,String sourceMedia);
}
