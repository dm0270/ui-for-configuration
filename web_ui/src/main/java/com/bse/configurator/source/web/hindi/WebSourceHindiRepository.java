package com.bse.configurator.source.web.hindi;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface WebSourceHindiRepository  extends JpaRepository<WebSourceHindi, Long>{
	@Query("select t from WebSourceHindi t where t.endDate='9999-12-31 23:59:59'")
	List<WebSourceHindi> findAllRecords();
}
