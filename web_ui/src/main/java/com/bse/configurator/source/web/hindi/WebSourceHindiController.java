package com.bse.configurator.source.web.hindi;

import java.util.List;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bse.configurator.source.SourceType;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping("/source/web/hindi")
public class WebSourceHindiController {
	private WebSourceHindiRepository webSourceHindiRepository;
	private SourceType sourceType;
	public WebSourceHindiController(WebSourceHindiRepository webSourceHindiRepository) {
		this.webSourceHindiRepository = webSourceHindiRepository;
		this.sourceType= new SourceType("rss","web_hindi");
	
	}

	@PostMapping
	public void addSource(@RequestBody WebSourceHindi source) {
		String userName = (String) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		source.setCreatedBy(userName);
		source.setSourceType(this.sourceType);
		this.webSourceHindiRepository.save(source);
	}

	@GetMapping
	public List<WebSourceHindi> getSources() {
		return this.webSourceHindiRepository.findAll();
	}
}
