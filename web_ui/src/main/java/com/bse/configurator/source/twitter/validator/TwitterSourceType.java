package com.bse.configurator.source.twitter.validator;

import javax.validation.Constraint;
import javax.validation.Payload;

import java.lang.annotation.*;

@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {TwitterSourceTypeValidator.class})
public @interface TwitterSourceType {
	String message() default "Invalid Source Type";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
